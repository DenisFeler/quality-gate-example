namespace MyNamespace;

public class MyClass
{
    private const string someString = "Testee";
    private readonly string _somethingElse = " or so";

    public MyClass()
    {
        this._somethingElse = "bla";
    }

    public void PrintSomething()
    {
        const string someString = "bla";
        Console.WriteLine(someString + this._somethingElse);
    }
}